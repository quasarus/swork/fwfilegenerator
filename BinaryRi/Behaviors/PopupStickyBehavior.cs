﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using System.Windows.Interop;
using System.Windows.Media;

namespace BinaryRi.Behaviors
{
    public class PopupStickyBehavior : Behavior<Popup>
    {
        Window ParentWindow;
        private bool Topmost = false;
        protected override void OnAttached()
        {
            
            ParentWindow = Window.GetWindow(AssociatedObject);
            
            if (ParentWindow != null)
            {
                ParentWindow.LocationChanged += PlacementWindow_LocationChanged;
                ParentWindow.SizeChanged += PlacementWindow_LocationChanged;

            }
            AssociatedObject.Opened += AssociatedObject_Opened;
        }
               

        protected override void OnDetaching()
        {
            if (ParentWindow != null)
            {
                ParentWindow.LocationChanged -= PlacementWindow_LocationChanged;
                ParentWindow.SizeChanged -= PlacementWindow_LocationChanged;
            }
            AssociatedObject.Opened -= AssociatedObject_Opened;
        }

        private void UpdateWindow()
        {
            var hwnd = ((HwndSource)PresentationSource.FromVisual(AssociatedObject.Child)).Handle;
            RECT rect;

            if (GetWindowRect(hwnd, out rect))
            {
                SetWindowPos(hwnd, Topmost ? -1 : -2, rect.Left, rect.Top, (int)AssociatedObject.Width, (int)AssociatedObject.Height, 0);
            }
        }

        private void AssociatedObject_Opened(object sender, EventArgs e)
        {
            UpdateWindow();
        }

        private void PlacementWindow_LocationChanged(object sender, EventArgs e)
        {
            var offset = AssociatedObject.HorizontalOffset;
            AssociatedObject.HorizontalOffset = offset + 1;
            AssociatedObject.HorizontalOffset = offset;
        }

        #region P/Invoke imports & definitions

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32", EntryPoint = "SetWindowPos")]
        private static extern int SetWindowPos(IntPtr hWnd, int hwndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        #endregion



    }
}
