﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace BinaryRi.Behaviors
{
    public class ListViewSelectionBehavior : Behavior<ListView>
    {
        protected override void OnAttached()
        {
            AssociatedObject.LostKeyboardFocus += AssociatedObject_LostKeyboardFocus;
            AssociatedObject.MouseDown += AssociatedObject_MouseDown;
        }

        private void AssociatedObject_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(AssociatedObject, e.GetPosition(AssociatedObject));
            
            if (r.VisualHit.GetType() != typeof(ListViewItem))
                AssociatedObject.UnselectAll();
        }

        private void AssociatedObject_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
        {
            AssociatedObject.UnselectAll();
        }

        
        protected override void OnDetaching()
        {
            
        }
    }
}
