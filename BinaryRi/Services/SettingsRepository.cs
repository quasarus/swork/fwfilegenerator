﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using BinaryRi.Model;

namespace BinaryRi.Services
{
    public class SettingsRepository : ISettingsRepository
    {
        
        private AppSettings Settings;
        private string SettingsFilePath = $"{Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location)}\\settings.xml";
        public AppSettings GetSettings()
        {
            try
            {
                Settings = DeserializeFile<AppSettings>(SettingsFilePath);
            }
            catch (Exception ex)
            {

                MessageBox.Show($"Не удалось получить настройки.\n{ex.Message}",
                    "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return Settings;
        }

        public void SaveSettings()
        {
            
            try
            {
                SerializeFile(Settings, SettingsFilePath);
            }
            catch (Exception ex)
            {

                MessageBox.Show($"Не удалось сохранить файл настроек.\n{ex.Message}",
                    "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        


        private AppSettings LoadAppSettings()
        {
            AppSettings settings = null;
            
            try
            {                
                if (!File.Exists(SettingsFilePath))
                {
                    settings = GetDefaultSettings();
                    SerializeFile(settings, SettingsFilePath);
                }
                else
                {
                    settings = DeserializeFile<AppSettings>(SettingsFilePath);
                }
                if (settings == null)
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"При работе с файлом настроек произошла ошибка.Приложение будет закрыто.\n{ex.Message}",
                    "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown(-1);
                return settings;
            }
            
            return settings;
        }

        private AppSettings GetDefaultSettings()
        {
            return new AppSettings()
            {
                BinFiles = new List<BinFile>(),
                RelPathIn = @"\",
                RelPathOut = @"\",
                Extension = "bin"
            };
        }

        public IEnumerable<string> GetBinFilesNames()
        {
            return Settings.BinFiles.Select(b => b.Name);
        }

        #region Сериализация/десериализация
        private T DeserializeFile<T>(string path)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StreamReader(path))
            {
                return (T)deserializer.Deserialize(reader);
            }
            
        }

        private void SerializeFile<T>(T data,string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (TextWriter writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, data);
            }
        }

        #endregion

        public object GetSettingValue(string settingName)
        {
            AppSettings sett = GetSettings();
            return sett.GetType().GetProperty(settingName).GetValue(sett, null);
        }

        public void SetSettingValue(string settingName, object val)
        {
            Settings.GetType().GetProperty(settingName).SetValue(Settings, val, null);
            SaveSettings();
        }

        public string GetRootDirectoryPath()
        {
            return Path.GetDirectoryName(SettingsFilePath);
        }

        public void InitializeSettings()
        {
            Settings = LoadAppSettings();
        }
    }
}
