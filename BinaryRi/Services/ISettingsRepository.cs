﻿using BinaryRi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Services
{
    public interface ISettingsRepository
    {
        void InitializeSettings();
        AppSettings GetSettings();

        void SaveSettings();

        string GetRootDirectoryPath();

        object GetSettingValue(string settingName);
        void SetSettingValue(string settingName, object val);

        IEnumerable<string> GetBinFilesNames();
    }
}
