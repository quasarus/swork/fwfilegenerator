﻿using BinaryRi.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BinaryRi.View
{
    /// <summary>
    /// Interaction logic for FileMapUserControl.xaml
    /// </summary>
    public partial class FileMapUserControl : UserControl
    {

        public ObservableCollection<AuxDataLoc> LocationData
        {
            get { return (ObservableCollection<AuxDataLoc>)GetValue(LocationDataProperty); }
            set { SetValue(LocationDataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AuxiliaryData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LocationDataProperty =
            DependencyProperty.Register("LocationData", typeof(ObservableCollection<AuxDataLoc>), typeof(FileMapUserControl),
                new PropertyMetadata(null, OnLocationDataPropertyChanged));

        private static void OnLocationDataPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //var auxData = (ObservableCollection<AuxDataLoc>)e.NewValue;
            //if (auxData == null)
            //{
            //    return;
            //}            
        }



        public FileMapUserControl()
        {
            InitializeComponent();
            this.Loaded += FileMapUserControl_Loaded;
        }

        private void FileMapUserControl_Loaded(object sender, RoutedEventArgs e)
        {

            CreateMap();
        }

        private void CreateMap()
        {
            if (LocationData == null)
            {
                return;
            }

            for (int cnt = 0; cnt < LocationData.Count; cnt++)
            {
                rootGrid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = new GridLength(0, GridUnitType.Auto)
                });
                ContentPresenter pr = new ContentPresenter();
                Grid.SetRow(pr, cnt);
                Grid.SetColumn(pr, 1);
                string templateName = "ButtonTemplateZoneFileBeginEnd";
                if (LocationData[cnt] is AuxDataLocBinFileBody)
                {
                    var fileBodyRow = rootGrid.RowDefinitions.Last();
                    fileBodyRow.Height = new GridLength(0.8, GridUnitType.Star);
                    templateName = "ButtonTemplateZoneFileBase";
                }
                pr.ContentTemplate = FindResource(templateName) as DataTemplate;
                pr.DataContext = LocationData[cnt];
                rootGrid.Children.Add(pr);
            }
        }
    }
}
