﻿using System.Windows.Controls;

namespace BinaryRi.View.Dialogs
{
    /// <summary>
    /// Interaction logic for UserAskDialogView
    /// </summary>
    public partial class UserAskDialogView : UserControl
    {
        public UserAskDialogView()
        {
            InitializeComponent();
        }
    }
}
