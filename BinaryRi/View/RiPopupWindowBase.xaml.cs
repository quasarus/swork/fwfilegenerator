﻿using Prism.Services.Dialogs;
using System;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace BinaryRi.View
{
    /// <summary>
    /// Interaction logic for RiPopupWindowBase.xaml
    /// </summary>
    public partial class RiPopupWindowBase : Window
    {
        private Window ParentWindow = null;
        private double ParentWindowMinWidth;
        private double ParentWindowMinHeight;

        public Type ParentWindowType { get; set; }
        public IDialogResult Result { get; set; }

        
        public RiPopupWindowBase()
        {
            InitializeComponent();
            ParentWindowType = typeof(Shell);
            this.Loaded += RiPopupWindowBase_Loaded;
            
        }

        private void RiPopupWindowBase_Loaded(object sender, RoutedEventArgs e)
        {
            Setup();
        }

        public void Setup()
        {
            ParentWindow = GetParentWindow(this.ParentWindowType);

            if (ParentWindow == null)
            {
                throw new Exception("Не установлен тип родительского окна для окна диалога.");
            }

            ParentWindowMinWidth = ParentWindow.MinWidth;
            ParentWindowMinHeight = ParentWindow.MinHeight;
            ParentWindow.MinWidth = this.Width + 50;
            ParentWindow.MinHeight = this.Height + 50;            
            SetToCenterOfParent();
            SetParentWindowEffects();
            ParentWindow.LocationChanged += ParentWindow_LocationChanged;
            ParentWindow.SizeChanged += ParentWindow_SizeChanged;
            this.Closing += RiPopupWindowBase_Closing;            
        }

        private void SetParentWindowEffects()
        {
            ParentWindow.IsEnabled = false;
            BlurApply(ParentWindow, 3, TimeSpan.FromMilliseconds(1), TimeSpan.Zero);
        }

        private void RiPopupWindowBase_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ParentWindow.MinWidth = ParentWindowMinWidth;
            ParentWindow.MinHeight = ParentWindowMinHeight;
            ParentWindow.IsEnabled = true;
            BlurDisable(ParentWindow, TimeSpan.FromMilliseconds(1), TimeSpan.Zero);
            ParentWindow.LocationChanged -= ParentWindow_LocationChanged;
            ParentWindow.SizeChanged -= ParentWindow_SizeChanged;
            this.Closing -= RiPopupWindowBase_Closing;
        }

        private void ParentWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetToCenterOfParent();
        }

        private void ParentWindow_LocationChanged(object sender, EventArgs e)
        {
            SetToCenterOfParent();
        }

        private void SetToCenterOfParent()
        {
            double lftOffs = (ParentWindow.Width - this.Width) / 2;
            double topOffs = (ParentWindow.Height - this.Height) / 2;
            this.Left = ParentWindow.Left + lftOffs;
            this.Top = ParentWindow.Top + topOffs;
        }

        private Window GetParentWindow(Type parentWindowType)
        {
            foreach (var window in Application.Current.Windows)
            {
                if (window.GetType() == parentWindowType)
                {
                    return (Window)window;
                }
            }
            return null;
        }


        public static void BlurApply(UIElement element,
        double blurRadius, TimeSpan duration, TimeSpan beginTime)
        {
            
            BlurEffect blur = new BlurEffect() { Radius = 0 };
            DoubleAnimation blurEnable = new DoubleAnimation(0, blurRadius, duration)
            { BeginTime = beginTime };
            element.Effect = blur;
            blur.BeginAnimation(BlurEffect.RadiusProperty, blurEnable);
        }

        public static void BlurDisable(UIElement element, TimeSpan duration, TimeSpan beginTime)
        {
            BlurEffect blur = element.Effect as BlurEffect;
            if (blur == null || blur.Radius == 0)
            {
                return;
            }
            DoubleAnimation blurDisable = new DoubleAnimation(blur.Radius, 0, duration) { BeginTime = beginTime };
            blur.BeginAnimation(BlurEffect.RadiusProperty, blurDisable);
        }

    }
}
