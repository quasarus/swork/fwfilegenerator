﻿using BinaryRi.Services;
using BinaryRi.Validators;
using CommonServiceLocator;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class SettingsItem : BindableBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    
}
