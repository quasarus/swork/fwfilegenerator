﻿using BinaryRi.Events;
using CommonServiceLocator;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class AuxDataLoc: BindableBase
    {
        public int Id { get; set; }
        
        public DelegateCommand LocationClickedCommand { get; set; }

        private ObservableCollection<AuxDataStrategy> _strategis;
        public ObservableCollection<AuxDataStrategy> Strategis
        {
            get { return _strategis; }
            set { SetProperty(ref _strategis, value); }
        }


        private int _offset;

        public int Offset
        {
            get { return _offset; }
            set 
            {
                _offset = value;
                OffsetStr = $"0x{value.ToString("X8")}";
            }
        }


        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private int _uiHeight;
        public int UiHeight
        {
            get { return _uiHeight; }
            set { SetProperty(ref _uiHeight, value); }
        }

        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get { return _isPopupOpen; }
            set 
            {
                SetProperty(ref _isPopupOpen, value);
                //if (value)
                //{
                //    _eventAggregator?.GetEvent<LocPopupOpenEvent>().Publish(Id);
                //}
            }
        }

        private string _offsetStr;
        public string OffsetStr
        {
            get { return _offsetStr; }
            set 
            { 
                SetProperty(ref _offsetStr, value);
            }
        }

        public AuxDataLoc()
        {
            Strategis = new ObservableCollection<AuxDataStrategy>();
            //_eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
            //_eventAggregator?.GetEvent<LocPopupOpenEvent>().Subscribe((id) =>
            //{
            //    IsPopupOpen = (id != Id) ? false : IsPopupOpen;
            //});
            Strategis.Add(new AuxDataStrategyInsertByOffset() { IsSelected = true});
            Strategis.Add(new AuxDataStrategyAddToBegin());
            Strategis.Add(new AuxDataStrategyAddToEnd());

            Offset = 256;
            LocationClickedCommand = new DelegateCommand(OnLocationClickedCommand);
        }

        

        

        private void OnLocationClickedCommand()
        {
            IsPopupOpen = !IsPopupOpen;
        }
    }

    public class AuxDataLocBinFileBody : AuxDataLoc
    {
        public AuxDataLocBinFileBody()
        {
            
            Description = "Тело бинарного файла";
        }
    }

    public class AuxDataLocBinFileBegin : AuxDataLoc
    {
        public AuxDataLocBinFileBegin()
        {
            Description = "Начало бинарного файла";
        }
    }

    public class AuxDataLocBinFileEnd : AuxDataLoc
    {
        public AuxDataLocBinFileEnd()
        {
            Description = "Конец бинарного файла";
        }
    }

}
