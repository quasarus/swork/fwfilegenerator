﻿using Prism.Mvvm;
using Prism.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class AuxDataStrategy : ValidatableBindableBase
    {        

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

    }

    public class AuxDataStrategyInsertByOffset : AuxDataStrategy
    {
        

        private string _offsetStr;
        public string OffsetStr
        {
            get { return _offsetStr; }
            set { SetProperty(ref _offsetStr, value); }
        }

        public int Offset { get; set; }

        public AuxDataStrategyInsertByOffset()
        {
            Description = "Добавить данные по указанному смещению";
        }
    }

    public class AuxDataStrategyAddToBegin : AuxDataStrategy
    {
        public AuxDataStrategyAddToBegin()
        {
            Description = "Добавить данные к началу";
        }

    }

    public class AuxDataStrategyAddToEnd : AuxDataStrategy
    {
        public AuxDataStrategyAddToEnd()
        {
            Description = "Добавить данные в конец";
        }
    }
}
