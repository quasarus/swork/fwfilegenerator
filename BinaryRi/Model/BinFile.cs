﻿
using BinaryRi.Events;
using BinaryRi.Validators;
using BinaryRi.View.Master;
using CommonServiceLocator;
using Prism.Commands;
using Prism.Common;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Validation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using System.Xml.Serialization;

namespace BinaryRi.Model
{
    
    public class BinFile : ValidatableBindableBase
    {
        protected private IRegionManager _regionManager;
        protected private IEventAggregator _eventAggregator;
        [XmlIgnore]
        public DelegateCommand<string> AuxFileCommand { get; set; }        


        [XmlIgnore]
        public DelegateCommand BinFileClickCommand { get; set; }
        private string _name;
        
        public string Name
        {
            get { return _name; }
            set
            {                
                SetProperty(ref _name, value);                
            }
        }

        private bool _isSelected;
        

        [XmlIgnore]
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {                                
                SetProperty(ref _isSelected, value);               
               
            }
        }
        


        public ObservableCollection<AuxData> AuxData { get; set; }


        public BinFile()
        {
            
            _regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            _eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
            AuxFileCommand = new DelegateCommand<string>((commandName) => 
            {
                _eventAggregator?.GetEvent<AuxFileEvent>()?.Publish(new AuxFileEventArgs(commandName, Name));                
            });

            //GetAuxDataInfoCommand = new DelegateCommand(()=>
            //{
            //    _eventAggregator?.GetEvent<AuxDataInfoShowEvent>()?.Publish(Name);
            //});
            //DeleteAuxDataCommand = new DelegateCommand(()=>
            //{
            //    _eventAggregator?.GetEvent<AuxDataDeleteEvent>()?.Publish(Name);
            //});
            //AddAuxDataCommand = new DelegateCommand(()=>
            //{
            //    _eventAggregator?.GetEvent<AuxDataAddEvent>()?.Publish(Name);
            //});
            BinFileClickCommand = new DelegateCommand(()=> 
            {
                IsSelected = !IsSelected;
            });
        }


        private void OnAddAuxDataCommand()
        {
            _regionManager?.RequestNavigate("BaseRegion",
                                new Uri(nameof(AuxDataTypeMasterView), UriKind.Relative));
        }

        
    }

    
}
