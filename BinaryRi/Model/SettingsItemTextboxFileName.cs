﻿using BinaryRi.Services;
using BinaryRi.Validators;
using CommonServiceLocator;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class SettingsItemTextboxFileName : SettingsItem, IDataErrorInfo
    {
        private protected IRegionManager _regionManager;
        private protected ISettingsRepository _settingsRepository;
        private protected IServiceLocator _serviceLocator;
        private GenericStringValidator _validator;



        private bool _isValid;
        public bool IsValid
        {
            get { return _isValid; }
            set { SetProperty(ref _isValid, value); }
        }
        private string _text;
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                var val = (string)GetType().GetProperty(columnName).GetValue(this, null);

                var valResult = _validator.Validate(val, new ValidationContext(this, _serviceLocator, null)
                {
                    MemberName = columnName,
                });

                if (string.IsNullOrEmpty(valResult))
                {
                    _settingsRepository.SetSettingValue(Name, val);
                }
                IsValid = string.IsNullOrEmpty(valResult) ? true : false;
                return valResult;
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                return null;
            }
        }

        public SettingsItemTextboxFileName(IRegionManager regionManager,
            ISettingsRepository settingsRepository, IServiceLocator serviceLocator, GenericStringValidator validator)
        {
            _serviceLocator = serviceLocator;
            _regionManager = regionManager;
            _settingsRepository = settingsRepository;
            _validator = validator ?? throw new Exception("Неверный валидатор");
        }
    }
}
