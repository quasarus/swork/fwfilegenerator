﻿using BinaryRi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class SettingsItemCheckBox : SettingsItem
    {
        private protected ISettingsRepository _settingsRepository;

        private bool _isChecked;
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _settingsRepository.SetSettingValue(Name, value);
                SetProperty(ref _isChecked, value);
            }
        }


        public SettingsItemCheckBox(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }
    }
}
