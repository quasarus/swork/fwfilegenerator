﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    

    
    public class AuxData : BindableBase
    {
        public string Name { get; set; }      


        //private List<AuxDataStrategy> _avStrategy;
        //public List<AuxDataStrategy> AvailableStrategies
        //{
        //    get { return _avStrategy; }
        //    set { SetProperty(ref _avStrategy, value); }
        //}

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        public AuxData()
        {
            //AvailableStrategies = new List<AuxDataStrategy>();
            
        }
    }

    public class AuxDataTypeCRC : AuxData
    {        
        public int CRC { get; set; }
        public AuxDataTypeCRC()
        {
            Name = "CRC исходного файла";            
                        
        }
    }

    public class AuxDataTypeLengh : AuxData
    {
        public int Lenght { get; set; }
        public AuxDataTypeLengh()
        {
            Name = "Длина исходного файла";
        }
    }

    public class AuxDataTypeRandom : AuxData
    {
        public int RndVal { get; set; }
        public AuxDataTypeRandom()
        {
            Name = "Случайное число";
        }
    }

    public class AuxDataTypeDateTime : AuxData
    {
        public string DateStr { get; set; }

        private bool _isCurrentDate;
        public bool IsCurrentDate
        {
            get { return _isCurrentDate; }
            set { SetProperty(ref _isCurrentDate, value); }
        }

        private DateTime _time;
        public DateTime Time
        {
            get { return _time; }
            set
            {
                SetProperty(ref _time, value);
                DateStr = Time.ToString("yyyy:MM:dd:HH:mm:ss");
            }
        }
        public AuxDataTypeDateTime()
        {
            IsCurrentDate = true;
            Name = "Дата/время";
        }

    }


    public class AuxDataTypeIncVersion : AuxData
    {
        public int Version { get; set; }

        private bool _isAutoIncrement;
        public bool IsAutoIncrement
        {
            get { return _isAutoIncrement; }
            set { SetProperty(ref _isAutoIncrement, value); }
        }
        public AuxDataTypeIncVersion()
        {
            Name = "Версия исходного файла";
        }
    }

    public class AuxDataTypeIncFlags : AuxData
    {
        public int Flags { get; set; }

        public List<Flag> FlagList { get; set; }

        public AuxDataTypeIncFlags()
        {
            Name = "Двоичные флаги";
            FlagList = new List<Flag>();
            for (int cnt = 0; cnt < (8 * sizeof(int)); cnt++)
            {
                FlagList.Add(new Flag()
                {
                    IsSet = false,
                    Offset = cnt
                });                
            }
            foreach (var flag in FlagList)
            {
                flag.PropertyChanged += Flag_PropertyChanged;
            }
        }

        private void Flag_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSet")
            {

            }
        }
        
    }

    public class AuxDataTypeBinary : AuxData
    {
        private string _pathToBin;
        public string PathToBin
        {
            get { return _pathToBin; }
            set { SetProperty(ref _pathToBin, value); }
        }
        public byte[] Data { get; set; }
        public AuxDataTypeBinary()
        {
            Name = "Файл";
        }
    }

    public class AuxDataTypeString : AuxData
    {
        private string _str;
        public string Str
        {
            get { return _str; }
            set { SetProperty(ref _str, value); }
        }

        public AuxDataTypeString()
        {
            Name = "Текстовая строка";
        }
    }

}
