﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    

    [Serializable]
    public class AppSettings
    {
        public string Extension { get; set; }
        public string RelPathIn { get; set; }
        public string RelPathOut { get; set; }
        public List<BinFile> BinFiles { get; set; }
                
    }
}
