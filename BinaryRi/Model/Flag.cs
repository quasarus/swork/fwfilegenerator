﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Model
{
    public class Flag: BindableBase
    {
        private bool _isSet;
        public bool IsSet
        {
            get { return _isSet; }
            set { SetProperty(ref _isSet, value); }
        }

        public int Offset { get; set; }

    }
}
