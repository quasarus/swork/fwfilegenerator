﻿using BinaryRi.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Validators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class CheckBinFileNameAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            string name = (string)value;
            
            if (string.IsNullOrEmpty(name))
            {
                return new ValidationResult("Введите имя файла");
            }
            if ((name.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0))
            {
                return new ValidationResult("Недопустимые символы в названии файла");
            }
            var settService = context.GetService(typeof(ISettingsRepository)) as ISettingsRepository;
            if (settService != null)
            {
                IEnumerable<string> binFilesNames = settService.GetBinFilesNames();
                if (binFilesNames.Contains(name))
                {
                    return new ValidationResult("Файл с таким именем уже существует");
                }
            }
            return ValidationResult.Success;
        }        
    }
}
