﻿using BinaryRi.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Validators
{
    public abstract class GenericStringValidator
    {
        public abstract string Validate(string str, ValidationContext context);        
    }

    public class RelPathValidator : GenericStringValidator
    {
        public override string Validate(string str, ValidationContext context)
        {
            
            var settService = context.GetService(typeof(ISettingsRepository)) as ISettingsRepository;
            
            if (!Directory.Exists(settService.GetRootDirectoryPath() + str))
            {
                return "Путь к папке с бинарными файлами не существует";
            }
            
            return string.Empty;
        }
    }

    public class ExtensionValidator : GenericStringValidator
    {
        public override string Validate(string str, ValidationContext context)
        {
            var settService = context.GetService(typeof(ISettingsRepository)) as ISettingsRepository;
            if ((str.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0))
            {
                return "Недопустимые символы в названии файла";
            }
            if (string.IsNullOrEmpty(str))
            {
                return "Введите значение";
            }
            return string.Empty;
        }
    }
}
