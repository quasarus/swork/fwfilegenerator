﻿using BinaryRi.Events;
using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.View;
using BinaryRi.View.Master;
using Prism.Commands;
using Prism.Events;
using Prism.Interactivity.InteractionRequest;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BinaryRi.ViewModel
{
    public class ShellViewModel : BindableBase
    {
        protected private ISettingsRepository _settingsRepository;
        
        protected private IRegionManager _regionManager;
        protected private IEventAggregator _eventAggregator;
        protected private IContainerExtension _containerExtension;
        
        public DelegateCommand ShellLoadedCommand { get; set; }


        private IRegion _region;

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        

        
        
        public ShellViewModel(IRegionManager regionManager,
            IContainerExtension containerExtension, IEventAggregator eventAggregator,
            IRegionNavigationService navService, ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _containerExtension = containerExtension;
            Title = "Конфигуратор";
            ShellLoadedCommand = new DelegateCommand(AfterShellLoading);
        }

        public void AfterShellLoading()
        {
            _settingsRepository.InitializeSettings();

            _region = _regionManager.Regions["BaseRegion"];
            
            _region.Add(_containerExtension.Resolve<BinFilesListView>(), nameof(BinFilesListView));
            _region.Add(_containerExtension.Resolve<BinFileMasterView>(), nameof(BinFileMasterView));
            _region.Add(_containerExtension.Resolve<SettingsView>(), nameof(SettingsView));
            _region.Add(_containerExtension.Resolve<AuxDataTypeMasterView>(), nameof(AuxDataTypeMasterView));
            _region.Add(_containerExtension.Resolve<AuxDataLocationMasterView>(), nameof(AuxDataLocationMasterView));
            _eventAggregator.GetEvent<ShellLoadedPubSubEvent>().Publish();

            _regionManager?.RequestNavigate("BaseRegion",
                                new Uri(nameof(BinFilesListView), UriKind.Relative));

        }











    }
}
