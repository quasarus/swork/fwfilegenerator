﻿using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.Validators;
using BinaryRi.View;
using CommonServiceLocator;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BinaryRi.ViewModel
{
    public class SettingsViewModel : BindableBase
    {
        private protected IRegionManager _regionManager;
        private protected ISettingsRepository _settingsRepository;
        private protected IServiceLocator _serviceLocator;

        private const string relPathInPropName = "RelPathIn";
        private const string relPathOutPropName = "RelPathIn";
        public DelegateCommand BackCommand { get; set; }

        private ObservableCollection<SettingsItem> _settItems;
        public ObservableCollection<SettingsItem> SettingsItems
        {
            get { return _settItems; }
            set { SetProperty(ref _settItems, value); }
        }


        public SettingsViewModel(IRegionManager regionManager,
            ISettingsRepository settingsRepository, IServiceLocator serviceLocator)
        {
            _regionManager = regionManager;
            _serviceLocator = serviceLocator;
            _settingsRepository = settingsRepository;
            BackCommand = new DelegateCommand(()=> 
            {
                _regionManager.RequestNavigate("BaseRegion",
                                new Uri(nameof(BinFilesListView), UriKind.Relative));
            });

            SettingsItems = new ObservableCollection<SettingsItem>();
            
            SettingsItems.Add(new SettingsItemTextboxFileName(_regionManager,
                _settingsRepository, _serviceLocator, new RelPathValidator())
            {
                Description = "Относительный путь к папке с исходными бинарными файлами:",
                Name = relPathInPropName,
                Text = (string)_settingsRepository.GetSettingValue(relPathInPropName)
            });
            SettingsItems.Add(new SettingsItemTextboxFileName(_regionManager,
                _settingsRepository, _serviceLocator, new RelPathValidator())
            {
                Description = "Относительный путь к папке с выходными бинарными файлами:",
                Name = relPathOutPropName,
                Text = (string)_settingsRepository.GetSettingValue(relPathOutPropName)
            });
            SettingsItems.Add(new SettingsItemTextboxFileName(_regionManager,
                _settingsRepository, _serviceLocator, new ExtensionValidator())
            {
                Description = "Расширение выходных файлов:",
                Name = "Extension",
                Text = (string)_settingsRepository.GetSettingValue("Extension")
            });
            

        }

        
    }
}
