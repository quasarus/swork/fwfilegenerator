﻿using BinaryRi.Events;
using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.View;
using BinaryRi.View.Dialogs;
using BinaryRi.View.Master;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.ViewModel
{
    public class BinFilesListViewModel : BindableBase, INavigationAware
    {
        public DelegateCommand AddBinFileCommand { get; set; }
        public DelegateCommand RemoveBinFilesCommand { get; set; }
        public DelegateCommand OpenSettingsCommand { get; set; }
        protected private IDialogService _dialogService;
        protected private IEventAggregator _eventAggregator;
        protected private ISettingsRepository _settingsRepository;
        protected private IRegionManager _regionManager;
        
        protected private IRegionNavigationService _navigationService;

        private ObservableCollection<BinFile> _binFiles;
        public ObservableCollection<BinFile> BinFiles
        {
            get { return _binFiles; }
            set
            {
                SetProperty(ref _binFiles, value);
            }
        }

        


        public BinFilesListViewModel(IDialogService dialogService, ISettingsRepository settingsRepository,
            IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            _dialogService = dialogService;
            _eventAggregator = eventAggregator;
            _settingsRepository = settingsRepository;
            _regionManager = regionManager;            
            BinFiles = new ObservableCollection<BinFile>();
            _eventAggregator.GetEvent<NewBinFilePubSubEvent>().Subscribe(OnNewBinFileAdd);
            _eventAggregator.GetEvent<ShellLoadedPubSubEvent>().Subscribe(OnShellLoaded);
            _eventAggregator.GetEvent<AuxFileEvent>().Subscribe(OnFileEvent);            
            AddBinFileCommand = new DelegateCommand(OnAddBinFileCommand);
            RemoveBinFilesCommand = new DelegateCommand(OnRemoveBinFilesCommand);
            OpenSettingsCommand = new DelegateCommand(()=>
            {
                
                _regionManager.RequestNavigate("BaseRegion",
                                new Uri(nameof(SettingsView), UriKind.Relative));
            });
        }

        private void OnFileEvent(AuxFileEventArgs e)
        {
            //_navigationService.Journal.Clear();
            //_binFileOpContextRep.SetBinFileForOperation(e.FileName);
            var selectedFile = BinFiles.SingleOrDefault(v => v.Name == e.FileName);
            NavigationParameters navParam = new NavigationParameters();
            switch (e.CommandName)
            {
                case "Add":                    
                    navParam.Add("SelectedBinFile", selectedFile);                    
                    _regionManager?.RequestNavigate("BaseRegion",
                                new Uri(nameof(AuxDataTypeMasterView), UriKind.Relative), navParam);                    break;
                case "Delete":
                    break;
                case "Info":
                    break;
                default:
                    throw new Exception("Неизвестная команда для бинарного файла");
            }
        }


        

        private void OnRemoveBinFilesCommand()
        {
            if (BinFiles.All(f=>!f.IsSelected))
            {
                string message = "Не выбран ни один файл для удаления";
                _dialogService.Show("UserWarnDialogView",
                    new DialogParameters($"message={message}"), r => { });
            }
            else
            {
                var selectedFiles = BinFiles.Where(g => g.IsSelected);
                string message = $"Вы действительно хотите удалить {selectedFiles.Count()} файл(ов)?";
                _dialogService.Show("UserAskDialogView",
                    new DialogParameters($"message={message}"), r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            foreach (var file in selectedFiles)
                            {
                                _settingsRepository.GetSettings().BinFiles.RemoveAll(f => f.Name == file.Name);
                                _settingsRepository.SaveSettings();
                            }
                            UpdateBinFiles();
                        }
                    });
                
            }
        }

        private void OnShellLoaded()
        {
            UpdateBinFiles();
        }

        private void OnAddBinFileCommand()
        {
            var message = "This is a message that should be shown in the dialog.";

            _dialogService.Show(nameof(BinFileNameDialogView), new DialogParameters($"message={message}"), r =>
            {
            });
        }

        private void OnNewBinFileAdd(BinFile file)
        {
            _settingsRepository.GetSettings().BinFiles.Add(file);
            _settingsRepository.SaveSettings();
            UpdateBinFiles();
        }

        

        private void UpdateBinFiles()
        {
            BinFiles = new ObservableCollection<BinFile>(_settingsRepository.GetSettings().BinFiles);
        }

        /// <summary>
        /// При возврате при навигации на страницу с списком файлов
        /// </summary>
        /// <param name="navigationContext"></param>
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            _navigationService = navigationContext.NavigationService;
            //navigationContext.NavigationService.Journal.Clear();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        /// <summary>
        /// При переходе при навигации со страницы со списком файлов
        /// </summary>
        /// <param name="navigationContext"></param>
        public void OnNavigatedFrom(NavigationContext navigationContext)
        {            
            //navigationContext.Parameters.Add("SelectedBinFile",)
        }
    }
}
