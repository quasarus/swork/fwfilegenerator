﻿using BinaryRi.Events;
using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.Validators;
using CommonServiceLocator;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.ViewModel
{
    public class BinFileNameDialogViewModel : DialogViewModelBase,IDataErrorInfo
    {
        private IEventAggregator _eventAggregator;
        IServiceLocator _serviceLocator;
        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        
        private string fileName;
        [CheckBinFileName]
        public string FileName
        {
            get { return fileName; }
            set
            {
                SetProperty(ref fileName, value);
                
                
            }
        }


        private bool _isRight;
        public bool IsRight
        {
            get { return _isRight; }
            set { SetProperty(ref _isRight, value); }
        }

        

        

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                var validationsResults = new List<ValidationResult>();
                var results = new List<ValidationResult>(1);
                var val = (string)this.GetType().GetProperty(columnName).GetValue(this, null);               

                IsRight = Validator.TryValidateProperty(
                    val,
                    new ValidationContext(this, _serviceLocator, null)
                    {
                        MemberName = columnName,
                        
                    },
                    results);
                return results.Count > 0 ? results.ElementAt(0).ErrorMessage : string.Empty;
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {                
                return null;
            }
        }

        public BinFileNameDialogViewModel(IEventAggregator eventAggregator,
            IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            _eventAggregator = eventAggregator;
            IsRight = false;
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            Message = parameters.GetValue<string>("message");
        }
        
        public override void RaiseRequestClose(IDialogResult dialogResult)
        {
            if (dialogResult.Result == ButtonResult.OK && IsRight)
            {
                _eventAggregator?.GetEvent<NewBinFilePubSubEvent>().
                    Publish(new BinFile() { Name = FileName });
            }
            
            base.RaiseRequestClose(dialogResult);
        }

        
    }
}
