﻿using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.View.Master;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Navigation;

namespace BinaryRi.ViewModel.Master
{
    public class AuxDataTypeMasterViewModel : BindableBase, INavigationAware
    {
        
        private IRegionManager _regionManager;
        private IRegionNavigationService _navigationService;
        private NavigationContext _navigationToContext;
        public DelegateCommand GoBackCommand { get; private set; }
        public DelegateCommand GoNextCommand { get; private set; }
        private List<AuxData> _auxData;
        public List<AuxData> AuxiliaryData
        {
            get { return _auxData; }
            set { SetProperty(ref _auxData, value); }
        }
       

        public AuxDataTypeMasterViewModel(IRegionNavigationService navigationService ,
            IRegionManager regionManager)
        {
            GoBackCommand = new DelegateCommand(OnGoBackCommand);
            GoNextCommand = new DelegateCommand(OnGoNextCommand);
            _navigationService = navigationService;
            _regionManager = regionManager;
            
            AuxiliaryData = new List<AuxData>()
            {
                new AuxDataTypeCRC(),
                new AuxDataTypeLengh(),
                new AuxDataTypeRandom(),
                new AuxDataTypeDateTime(),
                new AuxDataTypeIncVersion(),
                new AuxDataTypeIncFlags(),
                new AuxDataTypeBinary(),
                new AuxDataTypeString()
            };
            AuxiliaryData.ElementAt(0).IsSelected = true;
        }

        private void OnGoNextCommand()
        {
            if (_navigationService.Journal.CanGoForward)
            {
                _navigationService.Journal.GoForward();
            }
            else
            {                
                _regionManager?.RequestNavigate("BaseRegion",
                                new Uri(nameof(AuxDataLocationMasterView), UriKind.Relative));
            }

            
        }

        private void OnGoBackCommand()
        {
            if (_navigationService.Journal.CanGoBack)
            {                
                _navigationService.Journal.GoBack();
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }


        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            var selectedAuxiliaryData = AuxiliaryData.SingleOrDefault(p => p.IsSelected);
            navigationContext.Parameters.Add("SelectedAuxDataType", selectedAuxiliaryData);

            foreach (var ctx in _navigationToContext.Parameters)
            {
                navigationContext.Parameters.Add(ctx.Key,ctx.Value);
            }
        }
        

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            _navigationToContext = navigationContext;
            _navigationService = navigationContext.NavigationService;            
        }


    }
}
