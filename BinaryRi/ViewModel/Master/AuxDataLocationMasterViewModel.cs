﻿using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.View.Master;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace BinaryRi.ViewModel.Master
{
    public class AuxDataLocationMasterViewModel : BindableBase, INavigationAware
    {
        
        private IRegionNavigationService _navigationService;
        private ISettingsRepository _settingsRepository;
        private NavigationContext _navigationToContext;
        public DelegateCommand GoBackCommand { get; private set; }
        public DelegateCommand GoNextCommand { get; private set; }

        private ObservableCollection<AuxDataLoc> _auxDataLoc;
        public ObservableCollection<AuxDataLoc> AuxDataLoc
        {
            get { return _auxDataLoc; }
            set { SetProperty(ref _auxDataLoc, value); }
        }

        //private ObservableCollection<AuxDataStrategy> _availableStrategies;
        //public ObservableCollection<AuxDataStrategy> AvailableStrategies
        //{
        //    get { return _availableStrategies; }
        //    set { SetProperty(ref _availableStrategies, value); }
        //}

        //private ObservableCollection<AuxData> _auxData;
        //public ObservableCollection<AuxData> AuxiliaryData
        //{
        //    get { return _auxData; }
        //    set { SetProperty(ref _auxData, value); }
        //}

        public AuxDataLocationMasterViewModel(ISettingsRepository settingsRepository)
        {            
            _settingsRepository = settingsRepository;
            //AvailableStrategies = new ObservableCollection<AuxDataStrategy>();
            GoBackCommand = new DelegateCommand(OnGoBackCommand);
            GoNextCommand = new DelegateCommand(OnGoNextCommand);
            AuxDataLoc = new ObservableCollection<AuxDataLoc>();
        }

        private void OnGoNextCommand()
        {
            
        }

        private void OnGoBackCommand()
        {
            if (_navigationService.Journal.CanGoBack)
            {
                _navigationService.Journal.GoBack();
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            _navigationToContext = navigationContext;
            _navigationService = navigationContext.NavigationService;
            var selBinFile = (BinFile)navigationContext.Parameters["SelectedBinFile"];
            var selBinFileAuxData = selBinFile.AuxData;
            int id = 0;
            if (selBinFileAuxData.Count == 0)
            {
                //List<AuxDataLoc> dataLoc = new List<AuxDataLoc>()
                //{
                //    new AuxDataLocBinFileBegin(){UiHeight = 70, Id = id++ },
                //    new AuxDataLocBinFileBody(){UiHeight = 200, Id = id++ },
                //    new AuxDataLocBinFileEnd(){UiHeight = 70, Id = id++ }
                //};

                AuxDataLoc = new ObservableCollection<AuxDataLoc>();
                AuxDataLoc.CollectionChanged += AuxDataLoc_CollectionChanged;
                AuxDataLoc.Add(new AuxDataLocBinFileBegin() { UiHeight = 70 });
                AuxDataLoc.Add(new AuxDataLocBinFileBegin() { UiHeight = 200 });
                AuxDataLoc.Add(new AuxDataLocBinFileBegin() { UiHeight = 70 });       
                
            }

        }

        private void AuxDataLoc_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (INotifyPropertyChanged item in e.OldItems)
                    item.PropertyChanged -= item_PropertyChanged;
            }
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                    item.PropertyChanged += item_PropertyChanged;
            }
        }

        private void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var sndr = (AuxDataLoc)sender;
            if (e.PropertyName == "IsPopupOpen" && sndr.IsPopupOpen)
            {
                foreach (var loc in AuxDataLoc)
                {
                    if (sndr != loc)
                    {
                        loc.IsPopupOpen = false;
                    }
                }

            }
            
        }
    }
}
