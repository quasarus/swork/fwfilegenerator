﻿
using BinaryRi.Model;
using BinaryRi.Services;
using BinaryRi.View;
using BinaryRi.View.Dialogs;
using BinaryRi.View.Master;
using BinaryRi.ViewModel;
using BinaryRi.ViewModel.Master;
using CommonServiceLocator;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace BinaryRi
{
    
    /// <summary>
    /// Interaction logic for App.xaml. Обязательно использовать родительский класс PrismApplication
    /// из пространства имен Prism.Unity
    /// </summary>
    public partial class App:PrismApplication
    {
        
        protected override Window CreateShell()
        {            
            return Container.Resolve<Shell>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {            
            
            containerRegistry.RegisterSingleton<ISettingsRepository, SettingsRepository>();

            containerRegistry.RegisterForNavigation<BinFilesListView, BinFilesListViewModel>();
            containerRegistry.RegisterForNavigation<BinFileNameDialogView, BinFileNameDialogViewModel>();
            containerRegistry.RegisterForNavigation<AuxDataTypeMasterView, AuxDataTypeMasterViewModel>();
            containerRegistry.RegisterForNavigation<AuxDataLocationMasterView, AuxDataLocationMasterViewModel>();

            containerRegistry.RegisterDialog<UserWarnDialogView, UserWarnDialogViewModel>();
            containerRegistry.RegisterDialog<UserAskDialogView, UserAskDialogViewModel>();
            containerRegistry.RegisterDialogWindow<RiPopupWindow>();
        }

        
        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();
            
            ViewModelLocationProvider.Register<Shell, ShellViewModel>();
            ViewModelLocationProvider.Register<SettingsView, SettingsViewModel>();            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            
            base.OnStartup(e);
            if (e.Args.Length > 0)
            {
                if (e.Args[0] == "-o")
                {
                    //TODO обработать файлы
                    Current.Shutdown(0);
                }
            }
        }

    }
}
