﻿using BinaryRi.Model;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRi.Events
{
    public class AuxFileEventArgs : EventArgs
    {
        public string CommandName { get; set; }
        public string FileName { get; set; }

        public AuxFileEventArgs(string commandName,string fileName)
        {
            CommandName = commandName;
            FileName = fileName;
        }
    }

    public class AuxFileEvent : PubSubEvent<AuxFileEventArgs>{}
}
